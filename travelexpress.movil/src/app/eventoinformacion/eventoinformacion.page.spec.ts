import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventoinformacionPage } from './eventoinformacion.page';

describe('EventoinformacionPage', () => {
  let component: EventoinformacionPage;
  let fixture: ComponentFixture<EventoinformacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventoinformacionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventoinformacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
