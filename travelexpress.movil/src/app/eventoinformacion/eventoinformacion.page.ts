import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { NavController} from '@ionic/angular';
import { Router } from '@angular/router';
import {  ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-eventoinformacion',
  templateUrl: './eventoinformacion.page.html',
  styleUrls: ['./eventoinformacion.page.scss'],
})
export class EventoinformacionPage  {

  argumento=null;
  modelo=null;
  codigo=null;
  nombre=null;
  rentas: any;
  // 
  fecha:string= '';
  myDate: string = new Date().toISOString();
   dias:number = 0;
   hora:string = '';
   pago:string= '';
   chofer:String='';
   
  constructor(public restProvider: RestService, public navCtrl: NavController,private activatedroute:ActivatedRoute,private  router: Router) { 
this.argumento=this.activatedroute.snapshot.paramMap.get('placa');
this.modelo=this.activatedroute.snapshot.paramMap.get('nombre');
this.codigo=this.activatedroute.snapshot.paramMap.get('codigo');

console.log(this.argumento);
console.log(this.modelo);
console.log(this.codigo);
this.obtenerNombre();
console.log("obtuvo nombre"+this.nombre);
this.ObtenerAuto();
  }
  //Obtenemos la Lista de Autos
  ObtenerAuto() {
    this.restProvider.obtenerAutoRenta(this.codigo,this.modelo).then(data => {
      this.rentas = data;
      console.log(this.rentas);
    });
  }
  obtenerFecha(renta){
    console.log(this.modelo);
    console.log(renta.numero_asientos);
    console.log(renta.precio)
    console.log(this.fecha);
    console.log(this.hora);
    console.log(this.chofer);
    
    this.router.navigate(['/fpagoevento',this.modelo,renta.numero_asientos,renta.precio,this.fecha,this.hora,this.chofer]);
  }
  //Obtenermos el nombre de Usuario
  obtenerNombre(){
   console.log( this.restProvider.obtenerDato());
   this.nombre=this.restProvider.obtenerDato();
  }
  

}