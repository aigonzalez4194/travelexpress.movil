import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViajeinformacionPage } from './viajeinformacion.page';

describe('ViajeinformacionPage', () => {
  let component: ViajeinformacionPage;
  let fixture: ComponentFixture<ViajeinformacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViajeinformacionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViajeinformacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
