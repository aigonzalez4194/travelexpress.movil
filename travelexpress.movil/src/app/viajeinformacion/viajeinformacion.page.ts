import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { NavController, ToastController} from '@ionic/angular';
import {  SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import {  ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-viajeinformacion',
  templateUrl: './viajeinformacion.page.html',
  styleUrls: ['./viajeinformacion.page.scss'],
})
export class ViajeinformacionPage  {

  
  argumento=null;
  modelo=null;
  codigo=null;
  nombre=null;
  rentas: any;
  
  // 

  fecha:string= '';
  myDate: string = new Date().toISOString();
   dias:number = 0;
   hora:string = '';
   //Obtener el lugar 
   lugar:string= '';

   //
   message:string=null;
   file:string=null;
   link:string=null;
   subject:string=null;
   numasientos:string=null;
   

  constructor(public restProvider: RestService, public navCtrl: NavController,private activatedroute:ActivatedRoute,private  router: Router, public platform   : Platform,public socialSharing: SocialSharing ) { 
this.argumento=this.activatedroute.snapshot.paramMap.get('placa');
this.modelo=this.activatedroute.snapshot.paramMap.get('nombre');
this.codigo=this.activatedroute.snapshot.paramMap.get('codigo');

console.log(this.argumento);
console.log(this.modelo);
console.log(this.codigo);
this.obtenerNombre();
console.log("obtuvo nombre"+this.nombre);
this.ObtenerAuto();
  }
  ObtenerAuto() {
    this.restProvider.obtenerAutoRenta(this.codigo,this.modelo).then(data => {
      this.rentas = data;
      console.log(this.rentas);
    });
  }
  obtenerFecha(renta){
    console.log(this.fecha);
    console.log(this.dias);
    console.log(this.hora);
    console.log(this.lugar);
    console.log(this.numasientos);
    console.log('Precio'+renta.precio);
    this.router.navigate(['/fpagoviaje',this.modelo,this.argumento,this.fecha,this.dias,this.hora,this.lugar,renta.precio,this.numasientos]);
  }
  obtenerNombre(){
   console.log( this.restProvider.obtenerDato());
   this.nombre=this.restProvider.obtenerDato();
  }

  share(){
    //this.socialSharing.share("Modelo"+this.modelo,"Fecha"+this.fecha+this.hora,this.file,this.link)
    this.socialSharing.share("Modelo: "+this.modelo+"Fecha: "+this.fecha+''+this.hora)
    .then(()=>{

    }
    ).catch(()=>{

    });
  }
}
