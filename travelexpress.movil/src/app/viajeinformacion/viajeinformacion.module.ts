import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ViajeinformacionPage } from './viajeinformacion.page';

const routes: Routes = [
  {
    path: '',
    component: ViajeinformacionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ViajeinformacionPage]
})
export class ViajeinformacionPageModule {}
