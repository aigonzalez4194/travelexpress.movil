import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpagoeventoPage } from './fpagoevento.page';

describe('FpagoeventoPage', () => {
  let component: FpagoeventoPage;
  let fixture: ComponentFixture<FpagoeventoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpagoeventoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpagoeventoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
