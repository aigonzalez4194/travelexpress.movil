import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FpagoeventoPage } from './fpagoevento.page';

const routes: Routes = [
  {
    path: '',
    component: FpagoeventoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FpagoeventoPage]
})
export class FpagoeventoPageModule {}
