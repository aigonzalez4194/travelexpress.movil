import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { NavController, ToastController} from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import {  ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fpagoevento',
  templateUrl: './fpagoevento.page.html',
  styleUrls: ['./fpagoevento.page.scss'],
})
export class FpagoeventoPage {
modelo=null;
numeroa=null;
precio=null;
fecha=null;
hora=null;
chofer=null;
nombre=null;
fpago=null;
  titular=null;
  numero=null;
  dia=null;
  ano=null;
  codigo=null;
  eventos: any[] = [];
  constructor(private activatedroute:ActivatedRoute,public restProvider: RestService,public toastCtrl: ToastController,private router: Router) {
    this.modelo=this.activatedroute.snapshot.paramMap.get('modelo');
    this.numeroa=this.activatedroute.snapshot.paramMap.get('numeroa');
    this.precio=this.activatedroute.snapshot.paramMap.get('precio');
    this.fecha=this.activatedroute.snapshot.paramMap.get('fecha');
    this.hora=this.activatedroute.snapshot.paramMap.get('hora');
    this.chofer=this.activatedroute.snapshot.paramMap.get('chofer');
console.log(this.modelo);
console.log(this.numeroa);
console.log(this.precio);
console.log(this.fecha);
console.log(this.hora);
console.log(this.chofer);
this.obtenerNombre();
console.log(this.nombre);
   }
  
   obtenerNombre(){
    console.log( this.restProvider.obtenerDato());
    this.nombre=this.restProvider.obtenerDato();
   }
   guardarEvento(){
    console.log(this.nombre);
    console.log(this.modelo);
    console.log(this.fecha);
    console.log(this.numeroa);
    console.log(this.chofer);
    console.log(this.precio);
    console.log(this.hora);
    console.log(this.fpago);
    console.log(this.titular);
    console.log(this.numero);
    console.log(this.ano);
    console.log(this.codigo);
  }
 
   ///Metodo Registrar Rentas
   Registro() {
    this.guardarEvento();
        this.restProvider.guardarEvento(this.nombre,this.modelo,this.fecha,this.numeroa,this.chofer,this.precio,this.hora,this.fpago,this.titular,this.numero,this.ano,this.codigo).then((data) =>  {
          
          //this.personas = data;
          this.eventos[0] = data;
          console.log(this.eventos[0]);
          if (this.eventos[0] == null){
            console.log('Error al guardar');
            this.Error();
          }else{
            console.log('Evento Guardado');
            this.Guardo();
            this.pasarPagina();
            } 
        },
        (error) =>{
          console.error(error);
        }
        );
      }
      //Metodo que devuelve el toast de error 
      async Error() {
        const toast = await this.toastCtrl.create({
          message: 'Error al guardar.',
          duration: 500,
          position: 'middle'
        });
        toast.present();
      }
      //Metodo que nos muestra un mensaje de Usuario exitodo cuando el usuario y clave son correctos
      async Guardo() {
        const toast = await this.toastCtrl.create({
          message: 'Guardo',
          duration: 500,
          position: 'middle'
        });
        toast.present();
      }
      //Metodo para regresar al login
      pasarPagina(){
        this.router.navigate(['/menu']);
      }
    
      

}
