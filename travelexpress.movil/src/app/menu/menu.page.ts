import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import {HomePage} from '../home/home.page';
import { RestService } from '../rest.service';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage  {
 
  constructor(private router: Router,public menu: MenuController, public restProvider: RestService) { 
  //this.nombre=this.activatedroute.snapshot.paramMap.get('nombre');
    //console.log(this.nombre);
    menu.enable(true);
    
    

  }
  pasarRenta(){
    this.router.navigate(['/renta']);
  }
  pasarViaje(){
    this.router.navigate(['/viaje']);
  }
  pasarEvento(){
    this.router.navigate(['/evento']);
  }
  salir(){
    this.router.navigate(['/home']);
  }



  

}
