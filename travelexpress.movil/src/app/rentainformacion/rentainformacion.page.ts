import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { NavController, ToastController} from '@ionic/angular';

import { Router } from '@angular/router';
import {  ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-rentainformacion',
  templateUrl: './rentainformacion.page.html',
  styleUrls: ['./rentainformacion.page.scss'],
})
export class RentainformacionPage {

  argumento=null;
  modelo=null;
  codigo=null;
  nombre=null;
  rentas: any;



  // 

  fecha:string= '';
  myDate: string = new Date().toISOString();
   dias:number = 0;
   hora:string = '';
   pago:string= '';
   
//Array en donde obtenemos las persona

  constructor(public restProvider: RestService, public navCtrl: NavController,private activatedroute:ActivatedRoute,private  router: Router) { 
this.argumento=this.activatedroute.snapshot.paramMap.get('placa');
this.modelo=this.activatedroute.snapshot.paramMap.get('nombre');
this.codigo=this.activatedroute.snapshot.paramMap.get('codigo');

console.log(this.argumento);
console.log(this.modelo);
console.log(this.codigo);
this.obtenerNombre();
console.log("obtuvo nombre"+this.nombre);
this.ObtenerAuto();
  }
  ObtenerAuto() {
    this.restProvider.obtenerAutoRenta(this.codigo,this.modelo).then(data => {
      this.rentas = data;
      console.log(this.rentas);
    });
  }
  obtenerFecha(){
    console.log(this.fecha);
    console.log(this.dias);
    console.log(this.hora);
    console.log(this.pago);
    this.router.navigate(['/fpago',this.fecha,this.dias,this.hora,this.pago,this.argumento,this.modelo,this.codigo]);
  }
  obtenerNombre(){
   console.log( this.restProvider.obtenerDato());
   this.nombre=this.restProvider.obtenerDato();
  }
  

}
