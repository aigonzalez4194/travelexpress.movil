import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpagoviajePage } from './fpagoviaje.page';

describe('FpagoviajePage', () => {
  let component: FpagoviajePage;
  let fixture: ComponentFixture<FpagoviajePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpagoviajePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpagoviajePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
