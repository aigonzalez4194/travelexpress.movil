import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FpagoviajePage } from './fpagoviaje.page';

const routes: Routes = [
  {
    path: '',
    component: FpagoviajePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FpagoviajePage]
})
export class FpagoviajePageModule {}
