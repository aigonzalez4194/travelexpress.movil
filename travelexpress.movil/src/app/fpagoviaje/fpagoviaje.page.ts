import { Component, OnInit } from '@angular/core';

import { RestService } from '../rest.service';
import { NavController, ToastController} from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import {  ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fpagoviaje',
  templateUrl: './fpagoviaje.page.html',
  styleUrls: ['./fpagoviaje.page.scss'],
})
export class FpagoviajePage  {
placa=null;
modelo=null;
fecha=null;
dias=null;
hora=null;
lugar=null;
fpago=null;
  titular=null;
  numero=null;
  dia=null;
  ano=null;
  codigo=null;
  nombre=null;
  precio=null;
  num=null;
  viajes: any[] = [];
  constructor(private activatedroute:ActivatedRoute,public restProvider: RestService,public toastCtrl: ToastController,private router: Router) {
    this.placa=this.activatedroute.snapshot.paramMap.get('placa');
  this.modelo=this.activatedroute.snapshot.paramMap.get('modelo');
  this.fecha=this.activatedroute.snapshot.paramMap.get('fecha');
  this.dias=this.activatedroute.snapshot.paramMap.get('dias');
  this.hora=this.activatedroute.snapshot.paramMap.get('hora');
  this.lugar=this.activatedroute.snapshot.paramMap.get('lugar');
  this.precio=this.activatedroute.snapshot.paramMap.get('precio');
  this.num=this.activatedroute.snapshot.paramMap.get('numero');
  this.obtenerNombre();
  console.log(this.nombre);
   }
   obtenerNombre(){
    console.log( this.restProvider.obtenerDato());
    this.nombre=this.restProvider.obtenerDato();
   }
   guardarViaje(){
    console.log(this.nombre);
    console.log(this.modelo);
    console.log(this.num);
    console.log(this.precio);
    console.log(this.fecha);
    console.log(this.hora);
    console.log(this.lugar);
    console.log(this.fpago);
    console.log(this.titular);
    console.log(this.numero);
    console.log(this.ano);
    console.log(this.codigo);
    
  }
  
   ///Metodo Registrar Rentas
   Registro() {
this.guardarViaje();
    this.restProvider.guardarViaje(this.nombre,this.modelo,this.num,this.precio,this.fecha,this.hora,this.lugar,this.fpago,this.titular,this.numero,this.ano,this.codigo).then((data) =>  {
      
      //this.personas = data;
      this.viajes[0] = data;
      console.log(this.viajes[0]);
      if (this.viajes[0] == null){
        console.log('Error al guardar');
        this.Error();
      }else{
        console.log('Viaje Guardado');
        this.Guardo();
        this.pasarPagina();
        } 
    },
    (error) =>{
      console.error(error);
    }
    );
  }
  //Metodo que devuelve el toast de error 
  async Error() {
    const toast = await this.toastCtrl.create({
      message: 'Error al guardar.',
      duration: 500,
      position: 'middle'
    });
    toast.present();
  }
  //Metodo que nos muestra un mensaje de Usuario exitodo cuando el usuario y clave son correctos
  async Guardo() {
    const toast = await this.toastCtrl.create({
      message: 'Guardo',
      duration: 500,
      position: 'middle'
    });
    toast.present();
  }
  //Metodo para regresar al login
  pasarPagina(){
    this.router.navigate(['/menu']);
  }

  
}
