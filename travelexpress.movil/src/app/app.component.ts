import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  //Menu de la Aplicacion Web
  public appPages = [
    {
      title: 'Inicio',
      url: '/menu',
      icon: 'home'
    },
    {
      title: 'Renta',
      url: '/renta',
      icon: 'logo-model-s'
    },
    {
      title: 'Viaje',
      url: '/viaje',
      icon: 'people'
    },
    {
      title: 'Evento',
      url: '/evento',
      icon: 'star-outline'
    }, 
    {
      title: 'Salir',
      url: '/home',
      icon: 'backspace'
    }
   
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }
  
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
 
}
