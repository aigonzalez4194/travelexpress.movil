import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { NavController, ToastController} from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-renta',
  templateUrl: './renta.page.html',
  styleUrls: ['./renta.page.scss'],
})
export class RentaPage  {
items:any;
  searchTerm: string = '';
  autos: any;
  codigo:number = 0;
  nombre:string = '';
  placa:string = '';
  constructor( public restProvider: RestService, public navCtrl: NavController,private  router: Router) {
     this.ObtenerListaAutos();
    
     
  }
//Metodo para btenener  la lista de Autos 
  ObtenerListaAutos() {
    this.restProvider.obtenerAutos().then(data => {
      this.autos = data;
      console.log(this.autos);
      this.items=this.autos;
 
      
    });
  }
 
  
  openNavDetailsPage(auto) {
   
    //console.log( { auto: auto });
  console.log(auto.codigo);
  auto.codigo;
  auto.modelo;
  auto.placa;
  
  this.router.navigate(['/rentainformacion',auto.placa,auto.modelo,auto.codigo]);  
  }

  //Metodo que recibe la palabra a filtrar en el filtro de busqueda 
  filterItems(searchTerm){

    return this.autos.filter((auto) => {
     return auto.modelo.toLowerCase().indexOf(
       searchTerm.toLowerCase()) > -1;
     });
    
    
    }
    //Metodo para filtar los datos 
    initializeItems(){
      this.items=this.autos;
    }
    getItems(ev:any){
      this.initializeItems();
      let val= ev.target.value;
   
      if(val && val.trim() != ''){
        this.items=this.items.filter((item)=>{
          return (item.modelo.toLowerCase().indexOf(val.toLowerCase()) >-1 );
        })
      }
   
    }
   

}
