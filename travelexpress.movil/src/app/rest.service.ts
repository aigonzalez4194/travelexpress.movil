import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers} from '@angular/http';
import { Http ,RequestOptions} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {
user:string='';
  apiUrl = 'http://192.168.1.210:8080/TravelExpress/srv/personas';
  constructor(public http: HttpClient , public httpt:Http) {
    console.log('Hello RestServiceProvider Provider');
    }
  obtenerAutos() {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/listaAutos').subscribe(data => {
        resolve(data);
        
      }, err => {
        console.log(err);
      });
    });
  }
  Login(username:string,clave: string) {
    this.user=username;
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/login?'+'username='+username+'&clave='+clave).subscribe(data => {
        resolve(data);
      

      }, err => {
        console.log(err);
      });
    });
  }
  obtenerDato(){
        return this.user;
  }
  obtenerAutoRenta(codigo:number,nombre:string) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/recibeAuto?'+'id='+codigo+'&modelo='+nombre).subscribe(data => {
        resolve(data);
      

      }, err => {
        console.log(err);
      });
    });
  }
  registroUsuario(usuario:string,clave: string,cedula:string,nombres:string,apellidos:string,direccion:string,email:string) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/guardarCliente?'+'usuario='+usuario+'&clave='+clave+'&cedula='+cedula+'&nombres='+cedula+'&nombres='+nombres+'&apellidos='+apellidos+'&direccion='+direccion+'&email='+email).subscribe(data => {
        resolve(data);
      

      }, err => {
        console.log(err);
      });
    });
  }
guardarRenta(usuario:string,modelo: string,placa:string,fecha:string,hora:string,dias:string,formaPago:string,titular:string,numero:string,ano:string,codigo:string) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/guardarRenta?'+'usuario='+usuario+'&modelo='+modelo+'&placa='+placa+'&fechaR='+fecha+'&hora='+hora+'&numDias='+dias+'&formPago='+formaPago+'&titularT='+titular+'&numeroT='+numero+'&anioT='+ano+'&codigoS='+codigo).subscribe(data => {
        resolve(data);
      

      }, err => {
        console.log(err);
      });
    });
  }
  guardarViaje(usuario:string,modelo: string,numeroA:string,costo:string,fecha:string,hora:string,lugar:string,formp:string,titular:string,numero:string,ano:string,codigo:string) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/guardarViaje?'+'usuario='+usuario+'&modelo='+modelo+'&numAsientos='+numeroA+'&costoViaje='+costo+'&fecha='+fecha+'&hora='+hora+'&lugar='+lugar+'&formPago='+formp+'&titularT='+titular+'&numeroT='+numero+'&anioT='+ano+'&codigoS='+codigo).subscribe(data => {
        resolve(data);
      

      }, err => {
        console.log(err);
      });
    });
  }
  
  guardarEvento(usuario:string,modelo:string,fecha:string,numAsientos:string,chofer:string,costoViaje:string,hora:string,formpago:string,titular:string,numero:string,ano:string,codigo:string) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/guardarEvento?'+'usuario='+usuario+'&modelo='+modelo+'&fecha='+fecha+'&numAsientos='+numAsientos+'&chofer='+chofer+'&costoViaje='+costoViaje+'&hora='+hora+'&formPago='+formpago+'&titularT='+titular+'&numeroT='+numero+'&anioT='+ano+'&codigoS='+codigo).subscribe(data => {
        resolve(data);
      

      }, err => {
        console.log(err);
      });
    });
  }



  saveUser(data) {
  
    let headers = new Headers({ 'Content-Type': 'application/json' });

    let options = new RequestOptions({ headers: headers });
    let data1={usuario:"raja",clave:"rj@gmail.com", cedula:"7896325410", nombres:"New Ashoknagar", apellidos: "dassa", direccion: "dsadsa" };
  return new Promise((resolve, reject) => {
    this.httpt.post(this.apiUrl+'/guardarCliente',["dsa","dsaads","sdadsa","adsdsa","options","sd"].toString,options)
      .subscribe(res => {
        resolve(res);
        console.log('hola');
        console.log(res);
      }, (err) => {
        reject(err);
      });
  });
}

}
