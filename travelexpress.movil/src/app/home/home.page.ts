import { Component } from '@angular/core';
import { RestService } from '../rest.service';
import { NavController, ToastController} from '@ionic/angular';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  //Array en donde obtenemos las persona
  personas: any[] = [];
  //Variable en donde recibimos el nombre de la interfaz
  nombre:string = '';
  invalido:string = '';
  //Variable en donde recibimos la clave de la interfaz
  clave:string = '';
  usuario:string = '';
  
  constructor( public menu: MenuController,public navCtrl: NavController, public restProvider: RestService, public toastCtrl: ToastController, private router: Router){
    this.menu.enable(false);
//Desabilitamos el menu en la pantalla de inicio
    
      
  }

  ObtenerLogin() {
    this.restProvider.Login(this.nombre,this.clave).then((data) =>  {
      console.log(this.nombre);
      console.log(this.clave);
      //this.personas = data;
      this.personas[0] = data;
      console.log(this.personas[0]);
      if (this.personas[0] == null){
        console.log('no entro');
        this.Incorrecto();
      }else{
        console.log('entro');
this.Correcto();
this.pasarPagina();
      } 
    },
    (error) =>{
      console.error(error);
    }
    );
  }
  //Metodo que nos muestra un mensaje de incorrecto cuando el usuario y clave son invalidos
  async Incorrecto() {
    const toast = await this.toastCtrl.create({
      message: 'Usuario o contraseña incorrectos.',
      duration: 500,
      position: 'middle'
    });
    toast.present();
  }
  //Metodo que nos muestra un mensaje de Usuario exitodo cuando el usuario y clave son correctos
  async Correcto() {
    const toast = await this.toastCtrl.create({
      message: 'Usuario Exitoso.',
      duration: 500,
      position: 'middle'
    });
    toast.present();
  }
  async RegistroCliente() {
    const toast = await this.toastCtrl.create({
      message: 'Registro Cliente.',
      duration: 500,
      position: 'middle'
    });
    toast.present();
  }
 pasarPagina(){
   this.router.navigate(['/menu']);
 }
 pasarRegistro(){
  this.router.navigate(['/registro']);
  this.RegistroCliente();
}
}
