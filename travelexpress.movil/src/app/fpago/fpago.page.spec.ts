import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FpagoPage } from './fpago.page';

describe('FpagoPage', () => {
  let component: FpagoPage;
  let fixture: ComponentFixture<FpagoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FpagoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FpagoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
