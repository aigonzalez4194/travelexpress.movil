import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute } from '@angular/router';
import { RestService } from '../rest.service';
import { NavController, ToastController} from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fpago',
  templateUrl: './fpago.page.html',
  styleUrls: ['./fpago.page.scss'],
})
export class FpagoPage {
  fecha=null;
  dias=null;
  hora=null;
  pago=null;
  nombre=null;
  fpago=null;
  titular=null;
  numero=null;
  dia=null;
  ano=null;
  codigo=null;
  placa=null;
  modelo=null;
  cod=null;
  rentas: any[] = [];
  constructor(private activatedroute:ActivatedRoute,public restProvider: RestService,public toastCtrl: ToastController,private router: Router) {
    this.fecha=this.activatedroute.snapshot.paramMap.get('fecha');
    this.dias=this.activatedroute.snapshot.paramMap.get('dias');
    this.hora=this.activatedroute.snapshot.paramMap.get('hora');
    this.pago=this.activatedroute.snapshot.paramMap.get('pago');
    this.placa=this.activatedroute.snapshot.paramMap.get('placa');
    this.modelo=this.activatedroute.snapshot.paramMap.get('nombre');
    this.cod=this.activatedroute.snapshot.paramMap.get('codigo');
    console.log(this.fecha);
    console.log(this.dias);
    console.log(this.hora);
    console.log(this.pago);
    this.obtenerNombre();
    console.log(this.nombre);

   }

   obtenerNombre(){
    console.log( this.restProvider.obtenerDato());
    this.nombre=this.restProvider.obtenerDato();
   }
   guardarRenta(){
     console.log(this.nombre);
     console.log(this.modelo);
     console.log(this.placa);
     console.log(this.fecha);
     console.log(this.hora);
     console.log(this.dias);
     console.log(this.pago);
     console.log(this.fpago);
     console.log(this.titular);
     console.log(this.numero);
     console.log(this.ano);
     console.log(this.codigo);
   }
   ///Metodo Registrar Guardar
  Registro() {
    this.guardarRenta();
    this.restProvider.guardarRenta(this.nombre,this.modelo,this.placa,this.fecha,this.hora,this.dias,this.fpago,this.titular,this.numero,this.ano,this.codigo).then((data) =>  {
      
      //this.personas = data;
      this.rentas[0] = data;
      console.log(this.rentas[0]);
      if (this.rentas[0] == null){
        console.log('Error al guardar');
        this.Error();
      }else{
        console.log('Renta Guardado');
        this.Guardo();
        this.pasarPagina();
        } 
    },
    (error) =>{
      console.error(error);
    }
    );
  }
  //Metodo que devuelve el toast de error 
  async Error() {
    const toast = await this.toastCtrl.create({
      message: 'Error al guardar.',
      duration: 500,
      position: 'middle'
    });
    toast.present();
  }
  //Metodo que nos muestra un mensaje de Usuario exitodo cuando el usuario y clave son correctos
  async Guardo() {
    const toast = await this.toastCtrl.create({
      message: 'Guardo',
      duration: 500,
      position: 'middle'
    });
    toast.present();
  }
  //Metodo para regresar al login
  pasarPagina(){
    this.router.navigate(['/menu']);
  }

  
}
