import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { RestService } from '../rest.service';
import { NavController, ToastController} from '@ionic/angular';
import { Router } from '@angular/router';




@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage {
 //Array en donde obtenemos las persona
 personas: any[] = [];
 //Variable en donde recuperamos el usuario
 usuario:string = '';
 //Variable en donde recuperamos la clave
 clave:string = '';
  //Variable en donde recuperamos la cedula
  cedula:string = '';
//Variable en donde  recuperamos los nombres
 nombres:string = '';
//Variable en donde  recuperamos los apellidos
apellidos:string = '';
 //Variable en donde  recuperamos la direccion
 direccion:string = '';
 //Variable en donde  recuperamos el correo
 email:string = '';


  constructor(public menu: MenuController, public restProvider: RestService, public toastCtrl: ToastController, private router: Router) { 
    this.menu.enable(false);
  }

  
  ///Metodo Registrar Personas 
  Registro() {
    this.restProvider.registroUsuario(this.usuario,this.clave,this.cedula,this.nombres,this.apellidos,this.direccion,this.email).then((data) =>  {
      console.log(this.usuario);
      console.log(this.clave);
      //this.personas = data;
      this.personas[0] = data;
      console.log(this.personas[0]);
      if (this.personas[0] == null){
        console.log('Error al guardar');
        this.Error();
      }else{
        console.log('Registro Guardado');
        this.Guardo();
        this.pasarPagina();
        } 
    },
    (error) =>{
      console.error(error);
    }
    );
  }
  //Metodo que devuelve el toast de error 
  async Error() {
    const toast = await this.toastCtrl.create({
      message: 'Error al guardar.',
      duration: 500,
      position: 'middle'
    });
    toast.present();
  }
  //Metodo que nos muestra un mensaje de Usuario exitodo cuando el usuario y clave son correctos
  async Guardo() {
    const toast = await this.toastCtrl.create({
      message: 'Registro Exitoso',
      duration: 500,
      position: 'middle'
    });
    toast.present();
  }
  //Metodo para regresar al login
  pasarPagina(){
    this.router.navigate(['/home']);
  }

  
}
