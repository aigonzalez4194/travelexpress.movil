import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
  { path: 'registro', loadChildren: './registro/registro.module#RegistroPageModule' },
  { path: 'renta', loadChildren: './renta/renta.module#RentaPageModule' },
  { path: 'viaje', loadChildren: './viaje/viaje.module#ViajePageModule' },
  { path: 'evento', loadChildren: './evento/evento.module#EventoPageModule' },
  { path: 'historial', loadChildren: './historial/historial.module#HistorialPageModule' },
  { path: 'rentainformacion/:placa/:nombre/:codigo', loadChildren: './rentainformacion/rentainformacion.module#RentainformacionPageModule' },
  { path: 'fpago/:fecha/:dias/:hora/:pago/:placa/:nombre/:codigo', loadChildren: './fpago/fpago.module#FpagoPageModule' },
  { path: 'viajeinformacion/:placa/:nombre/:codigo', loadChildren: './viajeinformacion/viajeinformacion.module#ViajeinformacionPageModule' },
  { path: 'eventoinformacion/:placa/:nombre/:codigo', loadChildren: './eventoinformacion/eventoinformacion.module#EventoinformacionPageModule' },
  { path: 'fpagoviaje/:modelo/:placa/:fecha/:dias/:hora/:lugar/:precio/:numero', loadChildren: './fpagoviaje/fpagoviaje.module#FpagoviajePageModule' },
  { path: 'fpagoevento/:modelo/:numeroa/:precio/:fecha/:hora/:chofer', loadChildren: './fpagoevento/fpagoevento.module#FpagoeventoPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
